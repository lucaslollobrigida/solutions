module.exports = binaryGap = (N) =>
  parseInt(N, 10).
  toString(2).
  replace(/^0+|0+$/g, '').
  split('1').
  reduce((a, b) => {
      return a.length > b.length ? a : b;
  }).length;
