module.exports = connectedCell = (matrix = []) => {
  const rows = matrix.length;
  const columns = rows && matrix[0].length;

  const lookupAdj = (row, column) => {
    if ((row < 0 || row >= rows) || (column < 0 || column >= columns) || matrix[row][column] == 0) {
      return 0;
    }
    matrix[row][column] = 0; //Evitar repetições
    return (
      lookupAdj(row - 1, column) + //Norte
      lookupAdj(row - 1, column - 1) + //Noroeste
      lookupAdj(row, column - 1) + //Oeste
      lookupAdj(row + 1, column - 1) + //Sudoeste
      lookupAdj(row + 1, column) + //Sul
      lookupAdj(row + 1, column + 1) + //Sudeste
      lookupAdj(row, column + 1) + //Leste
      lookupAdj(row - 1, column + 1) + //Nordeste
      1); //O próprio elemento
  }

  return matrix.reduce((max, row, rowIndex) => {
    const size = row.reduce((acc, column, colIndex) => {
      const colTotal = lookupAdj(rowIndex, colIndex);
      return colTotal > acc ? colTotal : acc;
    }, max);
    return size > max ? size : max;
  }, 0);
}