const connectedCells = require('./connectedCells');

const assert = (fn = () => 0, mock, expected) => {
  const value = fn(mock)
  value !== expected && console.log(`expected ${expected}, but got ${value}`)
};

assert(connectedCells, [
  [1, 1, 0, 0, 1],
  [0, 0, 1, 0, 1],
  [0, 0, 1, 0, 1],
  [1, 0, 0, 0, 1],
  [1, 0, 0, 0, 1]
], 5);

assert(connectedCells, [], 0);

assert(connectedCells, undefined, 0);

assert(connectedCells, [
  [1, 1, 0, 0],
  [0, 1, 1, 0],
  [0, 0, 1, 0],
  [1, 0, 0, 0]
], 5);

assert(connectedCells, [
  [1, 1, 0, 1],
  [0, 1, 0, 1],
  [0, 0, 0, 0],
  [1, 1, 1, 0]
], 3);

assert(connectedCells, [
  [1, 1, 0, 0, 0],
  [0, 1, 0, 0, 0],
  [0, 0, 1, 0, 1],
  [1, 0, 0, 0, 1],
  [0, 1, 0, 1, 1],
], 4);

assert(connectedCells, [
  [0, 1, 0],
], 1);