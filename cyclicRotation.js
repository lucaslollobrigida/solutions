getPosition = (len, destiny) => {
  if (destiny >= 0)
    return destiny;

  if ((len - destiny) <= len)
    return (len + destiny);
  
  return getPosition(len, destiny + len);
};

module.exports = cyclicRotation = (A, K) => {  
  return A.map((target, index, arr) => arr[getPosition(arr.length, index - K)]);
};
