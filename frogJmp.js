module.exports = frogJmp = (X, Y, D) => {
  return Math.floor((Y-X)/D + ((Y-X)%D == 0 ? 0 : 1));
};

console.log(frogJmp(5, 160, 25));
