function connectedCell(matrix = []) {
  const rows = matrix.length;
  const colums = rows && matrix[0].length;
  let max = 0;

  const checkElement = (j, k) => {
    if (j < 0 || k < 0 || j >= rows || k >= colums || matrix[j][k] == 0) {
      return 0;
    }
    matrix[j][k] = 0; //Avitar repetições
    return (
      checkElement(j - 1, k) + //Norte
      checkElement(j - 1, k - 1) + //Noroeste
      checkElement(j, k - 1) + //Oeste
      checkElement(j + 1, k - 1) + //Sudoeste
      checkElement(j + 1, k) + //Sul
      checkElement(j + 1, k + 1) + //Sudeste
      checkElement(j, k + 1) + //Leste
      checkElement(j - 1, k + 1) + //Nordeste
      1); //O próprio elemento
  }

  for (let j = 0; j < rows; j++) {
    for (let k = 0; k < colums; k++) {
      if (matrix[j][k] == 1) {
        let size = checkElement(j, k);
        if (size > max) max = size;
      }
    }
  }
  return max;
}
const result = connectedCell([
  [1, 1, 0, 0, 1],
  [0, 0, 1, 0, 1],
  [0, 0, 1, 0, 1],
  [1, 0, 0, 0, 1],
  [1, 0, 0, 0, 1]
]);

console.log(result);