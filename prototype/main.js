const person = {
	greet: function() {
		console.log(`Hello, I'm ${this.name}`);
	},
	helper: function() {
		console.log(this);
	},
	setName: function(name) {
		this.name = name;
	},
	getName: function() {
		return this.name;
	}
}

const lucas = Object.assign(person)//, { name: 'Lucas'});

lucas.setName('Lucas');
console.log(lucas.getName());
lucas.greet();
lucas.helper();
