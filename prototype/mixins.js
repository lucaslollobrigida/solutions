const person = function() {
	const state = {};

	return Object.assign({
		set (name, age) {
			state['name'] = name;
			state['age'] = age;
		},
		get (stateName) {
			return state[stateName];
		},
		greet: function() {
			return `Hello, my name is ${state['name']} and I'm ${state['age']}`;
		},
		man: function() {
			console.log(this);
		}
	});
}

const createPerson = (model) => person.call(model);

const lucas = createPerson({});

lucas.set('Lucas', 22);
console.log(lucas.get('name'));
console.log(lucas.greet());
lucas.man();
