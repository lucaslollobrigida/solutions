const person = function() {
	let attr = {};

	this.set = function(name) {
		attr['name'] = name;
	}

	this.get = function() {
		return attr['name'];
	}

	this.greet = function() {
		console.log(`Hello, my name is ${attr['name']}`);
	}
	this.help = function() {
		console.log(this);
	}
}

const humam = function() {
	let attr = {};

	this.eat = function(food) {
		console.log(`${attr['name']} eated the ${food}`);
	}
}

const lucas = {};
	
person.call(lucas);
humam.call(lucas);

lucas.set('Lucas');
console.log(lucas.get());
lucas.eat('pizza');

lucas.greet();
lucas.help();
