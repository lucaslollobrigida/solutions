const person = function(obj) {
	const state = { ...obj };

	return Object.assign({}, {
		set (name, age) {
			state['name'] = name;
			state['age'] = age;
		},
		get (stateName) {
			return state[stateName];
		},
		greet: function() {
			console.log(`Hello, my name is ${state['name']} and I'm ${state['age']}`);
		},
		man: function() {
			console.log(this);
		}
	});
}

const createPerson = model => person.bind(this, model).call({});

module.exports = createPerson;
