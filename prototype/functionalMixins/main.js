const createPerson = require('./mixins/person.js');

const lucas = createPerson({ name: 'lucas', age: 22 });
//lucas.set('lucas', 22);

const javair = createPerson({ name: 'javair', age: 43 });

lucas.greet();
javair.greet();
