#!/usr/bin/env python
def connectedCell(matrix):

    l1 = len(matrix)
    l2 = l1
    region = []
    max_size = 0
    mov1 = [-1, -1, 0, 1, 1,  1,  0, -1]
    mov2 = [ 0,  1, 1, 1, 0, -1, -1, -1]

    for i in range(l1):
        for j in range(l2):

            size = 0

            if matrix[i][j]:
                region.append([i,j])
                size += 1
                matrix[i][j] = 0

                while region:

                    pos = region.pop()
                    for z in range(8):

                        inx1 = pos[0]+mov1[z]
                        inx2 = pos[1]+mov2[z]
                        cond1 = -1<inx1<l1
                        cond2 = -1<inx2<l2
                        cond3 = cond1 and cond2 and matrix[inx1][inx2]

                        if cond3:
                            matrix[inx1][inx2] = 0
                            region.append([inx1, inx2])
                            size += 1

            max_size = [size, max_size][max_size>size]

    return max_size

print(connectedCell([1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 1]))