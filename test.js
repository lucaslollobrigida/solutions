const assets = require('./data');
const _ = require('lodash');

const getDepth = (asset, depth = 0) => {

  const parent = _.find(assets, (a => a._id === asset.folderId));

  return !asset.folderId ?
    depth :
    getDepth(parent, depth + 1);
}

const data =
  assets
    .map(asset => ({ ...asset, key: asset._id, depth: getDepth(asset) }))
    .sort((a, b) => a.depth - b.depth)
    .reduce((tree, asset) => {
      if (asset.depth === 0)
        return [...tree, asset];

      const folderIndex = tree.reduce((acc, parent, index) => (parent._id == asset.folderId ? index : acc), -1);
      if (!tree[folderIndex])
        return tree;

      tree[folderIndex].children = [...tree[folderIndex].children || [], asset];

      return tree;
    }, []);

console.log(data.map(d => ({ name: d.name, children: d.children })));
