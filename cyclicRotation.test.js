const cyclicRotation = require('./cyclicRotation');
const assert = require('./assert');

assert(
  cyclicRotation([3, 8, 9, 7, 6], 1),
  [6, 3, 8, 9, 7]
);

assert(
  cyclicRotation([1, 2, 3, 4], 4),
  [1, 2, 3, 4]
);

assert(
  cyclicRotation([3, 8, 9, 7, 6], 3),
  [9, 7, 6, 3, 8]
);

assert(
  cyclicRotation([3, 8], 7),
  [8, 3]
);

assert(
  cyclicRotation([3, 8, 2], 11),
  [8, 2, 3]
);
