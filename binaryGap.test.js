const binaryGap2 = require('./binaryGap');

const binaryGap = binaryGap2

if (binaryGap(1041) != 5) 
  throw new Error('test failed');

if (binaryGap(32) != 0) 
  throw new Error('test failed');
  
if (binaryGap(5) != 1) 
  throw new Error('test failed');

console.log('All tests passed');
