module.exports = toBinary = (number, remainer = []) => {
  if (number > 0) {
    const newNumber = Math.floor(number / 2) || 0;
    const newRemainer = [...remainer, number % 2 ] || remainer;
    return toBinary(newNumber, newRemainer);
  }
  return remainer
    .reverse()
    .join('');
}