const _ = require('lodash');

module.exports = assert = (a, b) => {
  const isEqual = _.isEqual(b, a);
  
  !isEqual && console.log(`expected ${b}, but got ${a}`)

  return isEqual;
};