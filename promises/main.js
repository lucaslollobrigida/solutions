const fn = (success = false) => {
	return new Promise((resolve, reject) => {
		if (success) {
			setTimeout(resolve, 5000);
		} else {
			setTimeout(reject, 4000);
		}
	});
}


fn(true)
	.then(() => console.log('Teste 1 sucesso'))
	.catch(() => console.log('Teste 1 falha'));

fn(false)
	.then(() => console.log('Teste 2 sucesso'))
	.catch(() => console.log('teste 2 falha'));
