module.exports = permMissingElement = (A) => {
  const mult = A.length + 1;
  const sum = mult * (1 + mult) / 2;
  return sum - A.reduce((a, b) => (a + b), 0);
};

console.log(permMissingElement([1, 3, 4, 5, 6, 7, 8]));
console.log(permMissingElement([]));