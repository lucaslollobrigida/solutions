int find(int node) {
    if (node == parent[node]) {
        return node;
    }
    return find(parent[node]);
}

int union(int adj, int cur) {
    int parent_adj = find(adj);
    int parent_cur = find(cur);
    if (parent_adj != parent_cur) {
        if (cell_cnt[parent_adj] > cell_cnt[parent_cur]) {
            parent[parent_cur] = parent_adj;
            cell_cnt[parent_adj] += cell_cnt[parent_cur];
        } else {
            parent[parent_adj] = parent_cur;
            cell_cnt[parent_cur] += cell_cnt[parent_adj];
        }
    }
    return max(cell_cnt[parent_adj], cell_cnt[parent_cur]);
}