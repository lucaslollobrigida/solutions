module.exports = oddOccurrencesInArray = (A) => A.reduce((a, b) => a ^ b);
