const EMPTY_FORM = {
  script: '',
  fields: [],
}

const state = {
  name: '',
  address: '',
  forms: [],
}

addForm = (prev) => {
  return Object.assign({}, prev, {
    forms: [
      ...prev.forms,
      EMPTY_FORM
    ]
  })
}

const newState = addForm(addForm(state));

console.log(newState);

