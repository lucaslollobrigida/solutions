module.exports = assets = [
  {
    "_id": "avBX4SfL5Y697PNMH",
    "name": "Blockbit",
    "unitId": "pBp932DwjRpd435fP",
    "unitName": "Blockbit",
    "numVulnerability": 0,
    "numCompliance": 0,
    "os": null,
    "type": "group",
    "address": null,
    "forms": null,
    "ignoredScripts": "group",
    "credential": null,
    "tags": [],
    "folderId": null,
    "owner": "p7CKPdmXkhSnzz2ys",
    "ownerName": "Developers",
    "createdAt": "2018-07-12T19:06:49.218Z",
    "updatedAt": "2018-07-12T19:47:03.487Z"
  },
  {
    "_id": "L9SnPC46aSHdP7whJ",
    "name": "Comercial",
    "unitId": "pBp932DwjRpd435fP",
    "unitName": "Blockbit",
    "numVulnerability": 0,
    "numCompliance": 0,
    "os": null,
    "type": "group",
    "address": null,
    "forms": null,
    "ignoredScripts": null,
    "credential": null,
    "tags": [],
    "folderId": "S8BxoZKRcKSqzFmr7",
    "owner": "p7CKPdmXkhSnzz2ys",
    "ownerName": "Developers",
    "createdAt": "2018-07-12T19:48:31.147Z",
    "updatedAt": "2018-07-12T19:48:31.147Z"
  },
  {
    "_id": "wuoaKoMLKzCRt2BzR",
    "name": "Desenvolvimento",
    "unitId": "pBp932DwjRpd435fP",
    "unitName": "Blockbit",
    "numVulnerability": 0,
    "numCompliance": 0,
    "os": null,
    "type": "group",
    "address": null,
    "forms": null,
    "ignoredScripts": null,
    "credential": null,
    "tags": [],
    "folderId": "S8BxoZKRcKSqzFmr7",
    "owner": "p7CKPdmXkhSnzz2ys",
    "ownerName": "Developers",
    "createdAt": "2018-07-12T19:47:45.982Z",
    "updatedAt": "2018-07-12T19:47:45.982Z"
  },
  {
    "_id": "JmYKk55jg9762sWgZ",
    "name": "Financeiro",
    "unitId": "pBp932DwjRpd435fP",
    "unitName": "Blockbit",
    "numVulnerability": 0,
    "numCompliance": 0,
    "os": null,
    "type": "group",
    "address": null,
    "forms": null,
    "ignoredScripts": null,
    "credential": null,
    "tags": [],
    "folderId": "S8BxoZKRcKSqzFmr7",
    "owner": "p7CKPdmXkhSnzz2ys",
    "ownerName": "Developers",
    "createdAt": "2018-07-12T19:48:11.359Z",
    "updatedAt": "2018-07-12T19:48:11.359Z"
  },
  {
    "_id": "QbWXY3kRnwzzHWqiv",
    "name": "Servidor",
    "unitId": "pBp932DwjRpd435fP",
    "unitName": "Blockbit",
    "numVulnerability": 0,
    "numCompliance": 0,
    "os": "Canonical Ubuntu Linux 12.04 LTS",
    "tags": [
      {
        "id": "QSHpcrEbYAxrpiDwef",
        "title": "a",
        "color": "#FFFFFF"
      },
      {
        "id": "uWwXqKh8c2kEXZzJsF",
        "title": "c",
        "color": "#FFFFFF"
      }
    ],
    "createdAt": "2018-07-04T13:14:01.973Z",
    "updatedAt": "2018-07-13T11:45:20.049Z",
    "interfaces": [
      {
        "ip": "192.168.25.200",
        "mac": "",
        "fqdn": ""
      },
      {
        "ip": "192.168.25.101",
        "mac": "",
        "fqdn": ""
      }
    ],
    "folderId": "S8BxoZKRcKSqzFmr7"
  },
  {
    "_id": "gATZ6qPq7SSzTgkYd",
    "name": "Suporte",
    "unitId": "pBp932DwjRpd435fP",
    "unitName": "Blockbit",
    "numVulnerability": 0,
    "numCompliance": 0,
    "os": null,
    "type": "group",
    "address": null,
    "forms": null,
    "ignoredScripts": null,
    "credential": null,
    "tags": [],
    "folderId": "S8BxoZKRcKSqzFmr7",
    "owner": "p7CKPdmXkhSnzz2ys",
    "ownerName": "Developers",
    "createdAt": "2018-07-12T19:48:03.722Z",
    "updatedAt": "2018-07-12T19:48:03.722Z"
  },
  {
    "_id": "ktrJstg7fTT3oWxRs",
    "name": "Rio de Janeiro",
    "unitId": "pBp932DwjRpd435fP",
    "unitName": "Blockbit",
    "numVulnerability": 0,
    "numCompliance": 0,
    "os": null,
    "type": "group",
    "address": null,
    "forms": null,
    "ignoredScripts": null,
    "credential": null,
    "tags": [],
    "folderId": "avBX4SfL5Y697PNMH",
    "owner": "p7CKPdmXkhSnzz2ys",
    "ownerName": "Developers",
    "createdAt": "2018-07-13T12:07:30.258Z",
    "updatedAt": "2018-07-13T12:07:30.258Z"
  },
  {
    "_id": "S8BxoZKRcKSqzFmr7",
    "name": "São Paulo",
    "unitId": "pBp932DwjRpd435fP",
    "unitName": "Blockbit",
    "numVulnerability": 0,
    "numCompliance": 0,
    "os": null,
    "type": "group",
    "address": null,
    "forms": null,
    "ignoredScripts": null,
    "credential": null,
    "tags": [],
    "folderId": "avBX4SfL5Y697PNMH",
    "owner": "p7CKPdmXkhSnzz2ys",
    "ownerName": "Developers",
    "createdAt": "2018-07-12T19:47:16.051Z",
    "updatedAt": "2018-07-12T19:47:16.051Z"
  },
  {
    "_id": "k7ctC4C4BNNQ4b3wA",
    "name": "Ubuntu",
    "unitId": "pBp932DwjRpd435fP",
    "unitName": "Blockbit",
    "numVulnerability": 0,
    "numCompliance": 0,
    "os": "Canonical Ubuntu Linux 12.04 LTS",
    "credential": "6G8q65aLbFrkZZqGM",
    "credentialName": "linux",
    "tags": [
      {
        "id": "QSHpcrEbYAxrpiDwef",
        "title": "a",
        "color": "#FFFFFF"
      }
    ],
    "createdAt": "2018-07-10T17:49:42.059Z",
    "updatedAt": "2018-07-13T11:45:26.194Z",
    "folderId": "gATZ6qPq7SSzTgkYd"
  },
  {
    "_id": "ffFTtxNvSZdKunk5k",
    "name": "CentOS",
    "unitId": "pBp932DwjRpd435fP",
    "unitName": "Blockbit",
    "numVulnerability": 0,
    "numCompliance": 0,
    "os": "CentOS 3",
    "tags": [
      {
        "id": "ZH6483Zr8KT8v6yTxK",
        "title": "b",
        "color": "#FFFFFF"
      }
    ],
    "createdAt": "2018-07-10T17:46:52.029Z",
    "updatedAt": "2018-07-13T11:45:01.054Z",
    "credential": "pCZ3QRo4gwrqMkebQ",
    "credentialName": "Windows",
    "interfaces": [
      {
        "ip": "192.168.25.100",
        "mac": "",
        "fqdn": ""
      }
    ],
    "folderId": "wuoaKoMLKzCRt2BzR"
  },
  {
    "_id": "PgCkNpJJ97ag7koJo",
    "name": "VCM",
    "unitId": "pBp932DwjRpd435fP",
    "unitName": "Blockbit",
    "numVulnerability": 0,
    "numCompliance": 0,
    "os": null,
    "type": "group",
    "address": null,
    "forms": null,
    "ignoredScripts": null,
    "credential": null,
    "tags": [],
    "folderId": "wuoaKoMLKzCRt2BzR",
    "owner": "p7CKPdmXkhSnzz2ys",
    "ownerName": "Developers",
    "createdAt": "2018-07-12T19:47:55.151Z",
    "updatedAt": "2018-07-12T19:47:55.151Z"
  }
];